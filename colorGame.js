// var colors = [
//     "rgb(255, 0, 0)",
//     "rgb(255, 255, 0)",
//     "rgb(0, 255, 0)",
//     "rgb(0, 255, 255)",
//     "rgb(0, 0, 255)",
//     "rgb(255, 0, 255)"
// ];
// var colors = generateRandomColors(6);

var numberOfSquares = 6;
// var colors = generateRandomColors(numberOfSquares);
var colors = [];
var pickedColor;
// var pickedColor = khetsaUmbala();
var squares = document.querySelectorAll(".square");
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector('#message');
var h1 = document.querySelector('h1');
var resetButton = document.querySelector('#newColors');

//--------REFACTOR-------
var easyBtn = document.querySelector("#easyBtn");
var hardBtn = document.querySelector("#hardBtn");
//---------END Of REFACTORED CONTENT------

//----!!!!THE REFACTOR!!!!!-------
var modeButtons = document.querySelectorAll(".mode");


init();

function init() {
    //mode buttons event listeners
    setUpModeButtons();
    setUpSquares();    
    reset();
}

function setUpModeButtons() {
    for(var i = 0; i < modeButtons.length; i++) {
        modeButtons[i].addEventListener("click", function() {
            //removd the class selected from both then appply only to selected
            modeButtons[0].classList.remove("selected");
            modeButtons[1].classList.remove("selected");
            this.classList.add("selected");
    
            //figure out how many squares to show
            this.textContent === "Easy" ? numberOfSquares = 3: numberOfSquares = 6;
            //pick a picked color
            //update page to refelct changes
            reset();
        });
    }
}

function setUpSquares() {
    for(var i = 0; i < squares.length; i++) {
        //add initial color to squares
        // squares[i].style.backgroundColor = colors[i];
    
        //add click listeners to squares
        squares[i].addEventListener("click", function(){
            var clickedColor = this.style.backgroundColor;
            //compare color to pickedColor
            if(clickedColor === pickedColor) {
                // alert("Correct");
                messageDisplay.textContent = "Correct!";
                resetButton.textContent = "Play Again?";
                changeColors(pickedColor);
                h1.style.backgroundColor = clickedColor;
            } else {
                this.style.backgroundColor =  "rgb(20, 17, 17)";
                messageDisplay.textContent = "Try Again";
                
            }
        });
    }
}

function reset() {
    resetButton.textContent = "New Colors";
    //generate all new 
    colors = generateRandomColors(numberOfSquares);
    //pick a new random coclor form array
    pickedColor = khetsaUmbala();
    //change color display to macth picked color
    colorDisplay.textContent = pickedColor;
    // this.textContent = "New Colors";
    resetButton.textContent = "New Colors";
    messageDisplay.textContent = "";
    //change color of sqauares
    for(var i = 0; i < squares.length; i++) {
        if(colors[i]) {
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];
        } else {
            squares[i].style.display = "none";    
        }
        
    }
    h1.style.backgroundColor = "steelblue";   
}

resetButton.addEventListener('click', function() {
    reset();
} );



// colorDisplay.textContent = pickedColor;



function changeColors(color) {
    //loop through all squares
    for(var i = 0; i < squares.length; i++) {
        //change ech color to match given color
        squares[i].style.backgroundColor = color;
    }
}

function khetsaUmbala() {
   //pick a random number
  var umBalaIndex = Math.floor(Math.random() * colors.length); 
  //thise colors comes from generateColors(6)
  return colors[umBalaIndex];
}

function generateRandomColors(num) {
    //make an array
    var arr = []
    //repeat num times
    for(var i = 0; i < num; i++) {
        //get random number an dpush into array
        arr.push(randomColor());
    }

    //return that array
    return arr;
}

function randomColor() {
    //generate the number
    var numLess256 = Math.floor(Math.floor() * 256);
    //pick a red from 0-255
    var r = Math.floor(Math.random() * 256);
    //pick a green from 0-255
    var g = Math.floor(Math.random() * 256);
    //pick a blue form 0-255
    var b = Math.floor(Math.random() * 256);
    return `rgb(${r}, ${g}, ${b})`;


}

// easyBtn.addEventListener("click", function(){
//     hardBtn.classList.remove("selected");
//     easyBtn.classList.add("selected");
//     numberOfSquares = 3;
//     colors = generateRandomColors(numberOfSquares);
//     pickedColor = khetsaUmbala();
//     colorDisplay.textContent = pickedColor;

//     for(var i = 0; i < squares.length; i++){
//         if(colors[i]){
//             squares[i].style.backgroundColor = colors[i];
//         } else {
//             squares[i].style.display = "none";
//         }
//     }
// });

// hardBtn.addEventListener("click", function(){
//     hardBtn.classList.add("selected");
//     easyBtn.classList.remove("selected");
//     numberOfSquares = 6;
//      colors = generateRandomColors(numberOfSquares);
//     pickedColor = khetsaUmbala();
//     colorDisplay.textContent = pickedColor;

//     for(var i = 0; i < squares.length; i++){
//             squares[i].style.backgroundColor = colors[i];
//             squares[i].style.display = "block";
        
//     }
// });

// resetButton.addEventListener('click', function() {
//     resetButton.textContent = "New Colors";
//     //generate all new 
//     colors = generateRandomColors(numberOfSquares);
//     //pick a new random coclor form array
//     pickedColor = khetsaUmbala();
//     //change color display to macth picked color
//     colorDisplay.textContent = pickedColor;
//     this.textContent = "New Colors";
//     messageDisplay.textContent = "";
//     //change color of sqauares
//     for(var i = 0; i < squares.length; i++) {
//         squares[i].style.backgroundColor = colors[i];
//     }
//     h1.style.backgroundColor = "steelblue";

// } );